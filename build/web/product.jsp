<%-- 
    Document   : product
    Created on : Jan 9, 2019, 10:10:31 AM
    Author     : viett
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<Products>
    <c:forEach var="product" items="products">
        <Product>
        <ProductID>${product.productID}</ProductID>
        <ProductName>${product.productName}</ProductName>
        <SubblierID>${product.subblierID}</SubblierID>
        <CategoryID>${product.categoryID}</CategoryID>
        <QuantityPerUnit>${product.quantityPerUnit}</QuantityPerUnit>
        <UnitPrice>${product.unitPrice}</UnitPrice>
        <UnitsInStock>${product.unitsInStock}</UnitsInStock>
        <UnitsOnOrder>${product.unitsOnOrder}</UnitsOnOrder>
        <ReorderLevel>${product.reorderLevel}</ReorderLevel>
        <Discontinued>${product.discontinued}</Discontinued>
        </Product>
    </c:forEach>
</Products>